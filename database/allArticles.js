/* eslint-disable prettier/prettier */
import Realm from 'realm';
export const DATA ="allArticles";
export const data={
    name:DATA,
    primaryKey :'id',
    properties :{
        id :'int',
        title:'string',
        desc:'string',
        image:'string'
    }
}
const databaseOptions ={
    path : 'Article.realm',
    schema : [data]
}
export const insertArticles =newArticle => new Promise((resolve,reject)=>{
    Realm.open(databaseOptions).then(realm=>{
        realm.write(()=>{
            realm.create(DATA,newArticle)
            resolve(newArticle)
        })
    }).catch((error)=>reject(error));
})
export const updateArticles =article => new Promise((resolve,reject)=>{
    Realm.open(databaseOptions).then(realm=>{
        realm.write(()=>{
           let updatingArticle = realm.objectForPrimaryKey(DATA,article.id);
           updatingArticle.title = article.title;
           updatingArticle.desc = article.desc;
           updatingArticle.image = article.image;
           resolve();
        })
    }).catch((error)=>reject(error));
})
export const deleteArticles =articleId => new Promise((resolve,reject)=>{
    Realm.open(databaseOptions).then(realm=>{
        realm.write(()=>{
           let deletingArticle = realm.objectForPrimaryKey(DATA,articleId)
           realm.delete(deletingArticle)
           resolve();
        })
    }).catch((error)=>reject(error));
})
export const queryAllArticles =() => new Promise((resolve,reject)=>{
    Realm.open(databaseOptions).then(realm=>{
        realm.write(()=>{
           let allArticlesQuery = realm.objects(DATA);
           resolve(allArticlesQuery);
        })
    }).catch((error)=>reject(error));
})
export default new Realm(databaseOptions);