/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React ,{useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Alert,
  Dimensions
} from 'react-native';

import {Text,Container, Header, Content, Button, Form ,Input,Item,Title,Label,Body} from 'native-base';

const Details = (props) => {
    const {navigation} =props;
    const item = props.navigation.getParam('item');

    return(
      <Container style={styles.container}>
        <Header style={{backgroundColor:'#8675a9'}}>
          <Body >
            <Title style={{alignSelf:'center'}}></Title>
          </Body>
        </Header>
        <Content>
           <Text style={styles.title}>{item.title}</Text>    
            <Image source={{uri: `data:image/png;base64,${item.image}`}}  style={{height: 200, width: null, flex: 1}}/>
   
            <Text style={{marginTop:40 , marginLeft:10}}>{item.desc}</Text>
          </Content>
          </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
    },
    title:{
      marginTop:15,
      fontSize:20,
      fontWeight:'bold',
      color : '#8675a9',
      textAlign:'center'
    },
});

export default Details;
