/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React ,{useState} from 'react';
import {
  StyleSheet,
  Image,
} from 'react-native';
import ImagePicker from 'react-native-image-picker'
import {Text,Container, Header, Content, Button, Form ,Input,Item,Title,Label,Body} from 'native-base';
import { updateArticles } from '../../database/allArticles';


const AddArticle = (props) => {
    const {navigation} =props;
    const item = props.navigation.getParam('item');
    const [title,setTitle] = useState(item.title);
    const [desc,setDesc] = useState(item.desc);
    const [image,setImage] = useState(item.image);

    const editAr=()=>{
      const newarticle={
        id : item.id,
        title: title,
        desc : desc,
        image:image
      }
      updateArticles(newarticle).then(
        navigation.navigate('allArticles',{ch:title})
      ).catch((error)=>{
        alert(`insertion ${error}`)
      })
    }
    const upload=()=>{
      const options={}
      ImagePicker.launchImageLibrary(options,response=>{
        setImage(response.data)
      })
    }
    return(
      <Container style={styles.container}>
        <Header style={{backgroundColor:'#8675a9'}}>
          <Body >
            <Title style={{alignSelf:'center'}}>Edit  Article</Title>
          </Body>
        </Header>
        <Button 
          rounded 
          block 
          large
          style={styles.buttonStyle}
          onPress={()=>{upload()}}  
            >
              <Text style={{size:30}}>upload image</Text>
            </Button>
        <Content>
        <Image source={{uri: `data:image/png;base64,${image}`}}  style={styles.imageStyle}/>
        <Form>
            <Item stackedLabel>
              <Label>title</Label>
              <Input  
              multiline={true}
              numberOfLines={5}
              value={title} 
              onChangeText={(e) => setTitle(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label>description</Label>
              <Input 
              multiline={true}
              numberOfLines={5}
              value={desc} 
              onChangeText={(e) => setDesc(e)}/>
            </Item>
          </Form>
          <Button 
              style={{alignSelf:'center',marginTop:40,marginBottom:30}}
              transparent
              onPress={()=>{editAr()}}  
            >
              <Text style={styles.title}>Apply </Text>
            </Button>
          </Content>
          </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
    },
    title:{
      marginTop:15,
      fontSize:30,
      fontWeight:'bold',
      color : '#8675a9',
      textAlign:'center'
    },
    buttonStyle:{
        flex:1,
        backgroundColor:'#8675a9', 
        marginTop:10,
        width:'70%',
        alignSelf:'center'
    },
    imageStyle:{
      marginTop:20,
      height: 200,
       width: null,
       flex: 1}
});

export default AddArticle;
