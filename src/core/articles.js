/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React,{useState , useEffect , useCallback} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  Alert,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Card ,
   Container, 
   Header, Right,
   Content, CardItem , Thumbnail , 
   Button, Text ,Left,Icon,Title,Body } from 'native-base';
import {insertArticles , updateArticles , deleteArticles , queryAllArticles} from '../../database/allArticles';
import realm from '../../database/allArticles'

const MainCources = (props) => {
    const {navigation} =props;
    var ch = props.navigation.getParam('ch');
    const [mountScreen , setMount] = useState(null)
    const [myArticles , setArticles] = useState(null)
    useEffect(() => {
      queryAllArticles().then((arti)=>{
        setArticles(arti)
      })
    },[ch,mountScreen]);

    const Item = ({ item, onPress, style }) => (
      <Card>
      <CardItem cardBody button={true}>
        <Image source={{uri: `data:image/png;base64,${item.image}`}}  style={{height: 200, width: null, flex: 1}}/>
      </CardItem>
      <CardItem button={true}
            onPress={()=>{navigation.navigate('details',{item:item})}}>
        <Left>
          <Body>
            <Text>{item.title}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Left>
          <Button 
          transparent
          onPress={()=>{navigation.navigate('edit',{item:item})}}
          >
            <Icon type="FontAwesome" name="edit" style={styles.icon} />
          </Button>
        </Left>
          <Right>
          <Button 
          transparent
          onPress={()=>{removeArticle(item.id)}}
          >
            <Icon type="FontAwesome" name="trash-o" style={styles.icon}/>
          </Button>
        </Right>
      </CardItem>
    </Card>
    );
    const removeArticle=(id)=>{
    Alert.alert(
      'delete',
      'delete this article',
      [
        {
          text :'no' ,onPress :()=>{},
          style :'cancel'
        },
        {
          text : 'yes' , onPress : ()=>{
            setMount(Math.floor(Date.now()/1000))
            deleteArticles(id).then().catch(error=>{
              alert(`failed to delete ${id} error ${error}`)
            })
          }
        }
      ],
      {cancelable : true}
    )
    }
    
    const renderItem = ({ item }) => {
        return (
          <Item
            item={item}
          />
        );
      };
    return(
        <Container>
        <Header style={{backgroundColor:'#8675a9'}}>
        <Left style={{flex:1}}>
        </Left>
          <Body style={{flex:0.5}}>
            <Title>Articles</Title>
          </Body>
          <Right tyle={{flex:1}}>
            <Icon type="FontAwesome" name="plus" style={{color:'white'}} onPress={()=>{navigation.navigate('addArticle')}} />
            </Right>
        </Header>
        <Content>
        <FlatList
        data={myArticles}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
         </Content>
      </Container>
        );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor : '#ffd5cd',
    },
    icon :{
      fontSize: 30 ,
      color:'#80443A'
    },
    buttonStyle:{
        flex:1,
        color:'#c3aed6', 
        marginTop:90
    },
    title:{
      fontSize:17,
      fontWeight:'bold',
      marginLeft:10,
     flexShrink: 1
    },
    text:{
      fontSize:15,
      marginLeft:50,
      marginTop:5,
    },
    cost:{
      fontSize:20,
      marginLeft:50,
      marginTop: 5,
      color:'#08A200'
    }
});

export default MainCources;
