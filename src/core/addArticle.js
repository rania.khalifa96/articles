/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React ,{useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Alert,
  Dimensions
} from 'react-native';
import ImagePicker from 'react-native-image-picker'
import {Text,Container, Header, Content, Button, Form ,Input,Item,Title,Label,Body} from 'native-base';
import {insertArticles , updateArticles , deleteArticles , queryAllArticles} from '../../database/allArticles';
import realm from '../../database/allArticles'

const windowWidth = Dimensions.get('window').width;
const AddArticle = (props) => {

    const {navigation} =props;
    const [title,setTitle] = useState(null);
    const [desc,setDesc] = useState(null);
    const [image,setImage] = useState(null);

    const addMemeber=()=>{
      const newarticle={
        id : Math.floor(Date.now()/1000),
        title: title,
        desc : desc,
        image:image
      }
      insertArticles(newarticle).then(
        navigation.navigate('allArticles',{ch:title})
      ).catch((error)=>{
        alert(`insertion ${error}`)
      })
    }
    const upload=()=>{
      const options={}
      ImagePicker.launchImageLibrary(options,response=>{
        setImage(response.data)
      })
    }
    return(
      <Container>
        <Header style={{backgroundColor:'#8675a9'}}>
          <Body >
            <Title style={{alignSelf:'center'}}>add Article</Title>
          </Body>
        </Header>
        <Button 
          rounded 
          block 
          large
          style={styles.buttonStyle}
          onPress={()=>{upload()}}  
            >
          <Text style={{fontSize:30}}>upload image</Text>
        </Button>
        <Content>
        <Image source={{uri: `data:image/png;base64,${image}`}}  style={{height: 200, width: null, flex: 1}}/>
        <Form>
            <Item stackedLabel>
              <Label>title</Label>
              <Input  
              multiline={true}
              numberOfLines={3}
              value={title} 
              onChangeText={(e) => setTitle(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label>description</Label>
              <Input 
              multiline={true}
              numberOfLines={3}
              value={desc} 
              onChangeText={(e) => setDesc(e)}/>
            </Item>
          </Form>
          <Button 
              style={{alignSelf:'center',marginTop:40,marginBottom:20}}
              transparent
              onPress={()=>{addMemeber()}}  
            >
              <Text style={styles.title}>Add</Text>
            </Button>
          </Content>
          </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor : '#ffd5cd'
    },
    title:{
      marginTop:15,
      fontSize:35,
      fontWeight:'bold',
      color : '#8675a9',
      textAlign:'center'
    },
    buttonStyle:{
        flex:1,
        backgroundColor:'#8675a9', 
        marginTop:90,
        width:'70%',
        alignSelf:'center'
    },
});

export default AddArticle;
