/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React , {useState, useEffect}from 'react';
import {
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Item , Form, Label , Input , Container, Content, Button, Text } from 'native-base';

const NameAuth = (props) => {
   
  const {navigation} =props;
  const [userName,setUserName] = useState(null);
  const [password,setPassword] = useState(null);
  const [warning,setWarning] = useState(null);

  const log = async () =>{
    try {
      const name = await AsyncStorage.getItem('@user')
      const pass = await AsyncStorage.getItem('@pass')
      if(name===userName && pass===password) {
        navigation.navigate('allArticles')
      }
      else{
      setWarning("Wrong Entry Try again ..")
      setPassword("")
      setUserName("")
    }
      } catch(e) {
      alert(`insertion ${e}`)
  }
}
   const signUpUser = () =>{
     navigation.navigate('Signup')
    }
    return(
      <Container style={styles.container}>
        <Content>
        <Text style={styles.title}> Login </Text>
        <Form style={{marginTop:50,paddingTop:50}}>
            <Item stackedLabel>
              <Label>userName</Label>
              <Input  value={userName} onChangeText={(e) => setUserName(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label>passowrd</Label>
              <Input value={password} onChangeText={(e) => setPassword(e)}/>
            </Item>
          </Form>
          <Text style={styles.war}>{warning}</Text>
          <Button 
          rounded 
          block 
          large
          style={styles.buttonStyle}
           onPress={()=>{log()}}
          >
              <Text style={styles.text}> LogIn </Text>
              </Button>
        <Text style={{textAlign:'center',marginTop:40}}>don't have an account</Text>
        <Button 
        style={{alignSelf:'center',marginTop:40}}
        transparent
        onPress={signUpUser}  
      >
            <Text  >Sign Up</Text>
          </Button>
          </Content>
      </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        // backgroundColor : '#ffd5cd'
    },
    buttonStyle:{
        flex:1,
        backgroundColor:'#8675a9', 
        marginTop:90
    },
    title:{
      marginTop:50,
      fontSize:70,
      fontWeight:'bold',
      color : '#8675a9',
    },
    war:{
      marginTop:10,
      fontSize:15,
      fontWeight:'bold',
      color : 'red',
    },
    text:{
        fontSize:25,
      fontWeight:'bold',
    }
});

export default NameAuth;
