/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React ,{useState} from 'react';
import {
  StyleSheet,
  Dimensions
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Text,Container, Header, Content, Button, Form ,Input,Item,Title,Label,Body} from 'native-base';

const windowWidth = Dimensions.get('window').width;
const SignUp = (props) => {

    const {navigation} =props;
    const [userName,setUserName] = useState(null);
    const [password,setPassword] = useState(null);
    const [rePassword,setrePassword] = useState(null);
    const [warning,setWarning] = useState(null);
    const register = async () => {
      try {
        if(password===rePassword){
        await AsyncStorage.setItem('@user', userName)
        await AsyncStorage.setItem('@pass', password)
          navigation.navigate('allArticles')
          }else{
            setWarning("Password don't match")
            setPassword('')
            setrePassword('')
          }
      } catch (e) {
        alert(`insertion ${e}`)
      }
    }
    return(
      <Container style={styles.container}>
        <Content>
        <Text style={styles.title}> Sign Up </Text>
        <Form style={{marginTop:50,paddingTop:50}}>
            <Item stackedLabel>
              <Label>userName</Label>
              <Input  value={userName} onChangeText={(e) => setUserName(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label>passowrd</Label>
              <Input value={password} onChangeText={(e) => setPassword(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label>passowrd</Label>
              <Input value={rePassword} onChangeText={(e) => setrePassword(e)}/>
            </Item>
          </Form>
          <Text style={styles.war}>{warning}</Text>
          <Button 
          rounded 
          block 
          large
          style={styles.buttonStyle}
           onPress={()=>{register()}}
          >
              <Text style={styles.text}> Sign up </Text>
              </Button>
          </Content>
          </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        // backgroundColor : '#ffd5cd'
    },
    buttonStyle:{
        flex:1,
        backgroundColor:'#8675a9', 
        marginTop:90
    },
    title:{
      marginTop:50,
      fontSize:70,
      fontWeight:'bold',
      color : '#8675a9',
    },
    war:{
      marginTop:10,
      fontSize:20,
      fontWeight:'bold',
      color : 'red',
    },
    text:{
        fontSize:25,
      fontWeight:'bold',
    }
});

export default SignUp;
