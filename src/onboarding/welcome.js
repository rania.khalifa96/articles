/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React from 'react';
import {
  StyleSheet,
} from 'react-native';

import { Container, Content, Button, Text } from 'native-base';

const welcome = (props) => {
    const {navigation} =props;
   const start = () =>{navigation.navigate('Login')}

    return(
      <Container style={styles.container}>
        <Content>
        <Text style={styles.title}> Welcome </Text>
          <Button 
          rounded 
          block 
          large
          style={styles.buttonStyle}
          onPress={start}   
          >
              <Text style={styles.text}> start </Text>
              </Button>
          </Content>
      </Container>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
    },
    buttonStyle:{
        flex:1,
        backgroundColor:'#8675a9', 
        marginTop:100
    },
    title:{
      marginTop:170,
      fontSize:70,
      fontWeight:'bold',
      color : '#8675a9',
    },
    text:{
        fontSize:25,
      fontWeight:'bold',
    }
});

export default welcome;
