/* eslint-disable semi-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator , createAppContainer } from 'react-navigation';
import Welcome from '../onboarding/welcome';
import Login from '../onboarding/logIn';
import Signup from '../onboarding/signUp';
import Articles from '../core/articles';
import addArticle from '../core/addArticle';
import editArticle from '../core/EditArticle';
import Details from '../core/Details'
const LoginStack = createStackNavigator(
  {
    Welcome: { screen: Welcome },
    Login: { screen: Login },
    Signup: { screen: Signup},
  },
  {
    initialRouteName: 'Welcome',
    headerMode: 'none',
  },
);
const articles = createStackNavigator(
  {
    allArticles: { screen: Articles },
    details : {screen : Details},
    edit: { screen: editArticle },
    addArticle: { screen: addArticle},
  },
  {
    initialRouteName: 'allArticles',
    headerMode: 'none',
  },
);

// Manifest of possible screens
const RootNavigator = createSwitchNavigator(
  {
    LoginStack: { screen: LoginStack },
    articles : { screen:  articles},
  },
  {
    initialRouteName: 'LoginStack',
    cardStyle: {
      // backgroundColor:
      //   DynamicAppStyles.navThemeConstants[colorScheme].mainThemeBackgroundColor,
    },
  },
);

 const AppNavigator = createAppContainer(RootNavigator);


export default  AppNavigator;
